var Bicicleta = function(id,color,modelo,ubicacion){
   this.id=id;
   this.color=color;
   this.modelo=modelo;
   this.ubicacion=ubicacion;
}
   Bicicleta.prototype.toString=function(){
       return 'id:'+this.id + " Color:"+this.color;
   }

    Bicicleta.allBicis=[];
    
    Bicicleta.add=function(aBici){
      Bicicleta.allBicis.push(aBici);
   }


   var a =new Bicicleta(1,'rojo','urbana',[4.628251, -74.065955]);
   var b =new Bicicleta(2,'blanca','urbana',[4.632536, -74.063831]);
   var c =new Bicicleta(3,'verde','urbana',[4.637653, -74.063305]);
   var d =new Bicicleta(4,'naranja','urbana',[4.645128, -74.061492]);


   
  
   Bicicleta.add(a);
   Bicicleta.add(b);
   Bicicleta.add(c);
   Bicicleta.add(d);


   Bicicleta.findById=function(aBiciId){
     var aBici=Bicicleta.allBicis.find(x=>x.id==aBiciId);
      if(aBici)
         return aBici;
      else
         throw new Error(`No existe bicicleta con el id ${aBiciId}`);

   } 
   
   Bicicleta.removeById=function(aBiciId){
      for(var i=0;i<Bicicleta.allBicis.length;i++){
         
         if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
          }  
      }

   }



   module.exports=Bicicleta;

