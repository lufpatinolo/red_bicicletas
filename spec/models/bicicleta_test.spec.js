var Bicicleta = require('../../models/bicicleta');

beforeEach(()=>{
    Bicicleta.allBicis=[];
});


describe('Bicicleta.allBicis', ()=>{
    it('Comienza vacia',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
   
});

describe('Bicicleta.add',()=>{
    it('Agregarmos una',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a =new Bicicleta(1,'rojo','urbana',[-33.421787, -70.618638]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById',()=>{
    it('Devolver la bici con id 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a =new Bicicleta(1,'rojo','urbana',[-33.421787, -70.618638]);
        var b =new Bicicleta(2,'verde','montaña',[-33.421787, -70.618638]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici=Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);

    
    });    


});   